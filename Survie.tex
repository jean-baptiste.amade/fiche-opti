\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{caption}
\usepackage{xcolor}
\usepackage{scrextend}
\usepackage{esint}
\usepackage{amssymb}
\usepackage[skins]{tcolorbox}
\tcbuselibrary{breakable}
\usepackage{cancel}
\usepackage{hyperref}
\usepackage{soul}
\usepackage{tabularx}
\usepackage{listings}
\usepackage[european, straightvoltages]{circuitikz}
%\usepackage{calrsfs} %pour les jolies lettres
\graphicspath{ {./Images/} }
%\setlength{\parindent}{0pt}A utiliser avec prudence. Empeche d'indenter les props defs not de maths
\geometry{hmargin=2.5cm, vmargin=2.5cm}
\title{Survie d'opti}
\author{Jb}
\everymath{\displaystyle}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\newcommand\vect[1]{\overrightarrow{#1}}
\newcommand\R{\mathbb R}
\newcommand\C{\mathbb C}
\newcommand\N{\mathbb N}
\newcommand\Z{\mathbb Z}
\newcommand\cplx[1]{\overrightarrow{\underline{#1}}}
\newcommand\grad[1]{\overrightarrow{grad}\,\left(#1\right)}
\newcommand\rot[1]{\overrightarrow{rot}\,\left(#1\right)}
\DeclareRobustCommand{\rchi}{\Large{\mathpalette\irchi\relax}}
\newcommand{\irchi}[2]{\raisebox{\depth}{$#1\chi$}} % inner command, used by \rchi
\newcommand\hsp{\hspace*{7mm}}

\newcommand\Warning{%
 \makebox[1.4em][c]{%
 \makebox[0pt][c]{\raisebox{.1em}{\small!}}%
 \makebox[0pt][c]{\color{red}\Large$\bigtriangleup$}}}%
 
\lstset{
  frame=tb,
  language=python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{red},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\begin{document}
\maketitle

\newtcolorbox{propbox}[2][]{colbacktitle=red!10!white, colback=red!10!white,coltitle=red!70!black,fonttitle=\bfseries, enhanced, breakable,
attach boxed title to top left={yshift=-2mm, xshift=3mm},
  title={#2},#1}
  
\newtcolorbox{defbox}[2][]{colbacktitle=blue!10!white, colback=blue!10!white,coltitle=blue!70!black,fonttitle=\bfseries, enhanced, breakable,
attach boxed title to top left={yshift=-2mm, xshift=3mm},
  title={Definition : #2},#1}

\newtcolorbox{corobox}[2][]{colbacktitle=green!10!white, colback=green!10!white,coltitle=black!40!green,fonttitle=\bfseries, enhanced,
attach boxed title to top left={yshift=-2mm, xshift=3mm},
  title={#2},#1}

\newtcolorbox{otherbox}[2][]{colbacktitle=black!10!white, colback=black!10!white,coltitle=black,fonttitle=\bfseries, enhanced,
attach boxed title to top left={yshift=-2mm, xshift=3mm},
  title={#2},#1}

\textit{Merci aux slides de révision de 2023 sur NoFist.}
\section{Definitions}
\begin{defbox}{Fonction coercive}
Let $f : S \to [-\infty, +\infty]$
\begin{itemize}
\item The (effective) \textbf{domain} of $f$ is $dom\,f = \{x \in S | f(x) < +\infty\}$	
\item $f$ is \textbf{proper} if $\forall\, x \in S, \; f(x) \ne -\infty$ and $dom\;f \ne \O$
\end{itemize}
\end{defbox}

\begin{defbox}{Fonction coercive}
$f$ est dite coercive si $f(x) \underset{||x|| \to +\infty}{\to} +\infty$	.
\end{defbox}

\section{Existence d'une solution : théorème de Weierstrass}
\begin{propbox}{Théorème de Weierstrass}
Let $S$ be a nonempty compact set of a Hilbert space $\mathcal H$.\\
Let $f : S \to ]-\infty, +\infty]$ be a proper lsc function.\\
Then, there exists $\hat x \in S$ such that
$$f(\hat x) = \underset{x \in S}{inf}\; f(x)$$
\textit{(Autrement dit, $f$ admet un minimiseur global sur $S$.)}\\
In addition, the set of minimizers of $f$ is a compact set.
\end{propbox}

\begin{propbox}{Weierstrass + coercivité}
Si $\mathcal H$ est de dimension finie, et $f$ est coercive, lsc et propre, alors l'ensemble des minimiseurs est un ensemble compact non vide.
\end{propbox}

\newpage
\section{Différentiabilité}
Soient $\mathcal H$ et $\mathcal K$ des espaces normés.\\
Soit $\mathcal C \subset \mathcal H$, avec $T : \mathcal C \to \mathcal K$ et $x \in \mathcal C$

\begin{defbox}{Gâteaux-différentiabilité}
$T$ est Gâteaux-différentiable en $x$ s'il existe $T'(x) \in \mathcal B(\mathcal H,\mathcal K)$ tel que :
$$\forall\,y \in \mathcal H, \; T'(x)y = \underset{\begin{array}{c}\alpha \to 0\\\alpha > 0\end{array}}{inf} \frac{T(x+\alpha y) - T(x)}{\alpha}$$	
\end{defbox}

\begin{defbox}{Fréchet-différentiabilité}
$T$ es Fréchet-différentiable en $x$ s'il existe $T'(x) \in \mathcal B(\mathcal H, \mathcal K)$ tel que :
$$\underset{\begin{array}{c}y \to 0\\y \in \mathcal H \backslash \{0\}\end{array}}{lim}\,\frac{||T(x+y)-T(x)-T'(x)y||}{||y||} = 0$$	
\end{defbox}
\begin{propbox}{}
$$\text{Fréchet} \implies \text{Gâteaux}$$	
\end{propbox}

\begin{otherbox}{Montrer que $f$ est Fréchet-différentiable}
\begin{itemize}
\item Si les dérivées partielles existent et sont continues, alors $f$ est Fréchet-différentiable\\et $\nabla f = \left(\begin{array}{c}\partial_1f\\\vdots\\\partial_nf\end{array}\right)$
\item On calcule $f(x+h) - f(x)$ et on essaye de l'écrire sous la forme $Lh + o(h)$ où $L$ est une matrice (qui peut dépendre de $x$)
\item "Par composition et combinaison de fonctions Fréchet différentiables"
\end{itemize}
\end{otherbox}

\begin{otherbox}{Montrer que $f$ n'est pas Fréchet-différentiable}
\begin{itemize}
\item $f$ n'est pas continue en $x$
\item $f$ n'est pas Gâteaux-différentiable
\item $f$ n'admet pas de dérivée dans une direction	(par exemple pas de dérivée partielle)
\end{itemize}
\end{otherbox}

\begin{propbox}{Condition suffisante d'existence d'un minimum local}
\begin{itemize}
\item En dimension 1, si $f$ est deux fois dérivable en $x$, $f(x+h) = f(x) + f'(x)h + \frac12f''(x)h^2$	\\ donc $x$ est un minimiseur local si et seulement si $f'(x) = 0$ et $f''(x) \ge 0$
\item En dimension supérieure, si $f$ est deux fois dérivable en $x$,\\
$f(x+h) = f(x) + \langle \nabla f(x)|h\rangle + \frac12 \langle h | \nabla^2f(x)h\rangle$ est un minimum local (strict) si $\nabla^2f(x)$ est semi-définie positive (coercive).
\end{itemize}
\end{propbox}

\section{Convexité}
\begin{defbox}{}
$f$ est dite convexe sur $E$ ssi :
$$\forall(x,y) \in E^2, \, \forall\,\alpha \in [0,1], \, f(\alpha x + (1-\alpha)y) \le \alpha f(x) + (1-\alpha) f(y)$$	
\end{defbox}

\begin{otherbox}{Montrer qu'une fonction est convexe}
\begin{itemize}
\item La définition
\item Si $f$ est dérivable, montrer que la dérivée est croissante\\
(en dimension supérieure, cela s'écrit $\langle \nabla f(y) - \nabla f(x) | y-x\rangle \ge 0$)
\item Si $f$ est deux fois dérivable, montrer que la dérivée seconde est positive\\
(en dimension supérieure, cela s'écrit $\nabla^2f$ est symétrique définie positive, \\ie. $\langle z | \nabla^2f(x)z\rangle \ge 0$)	
\item $f$ est au-dessus de ses tangentes, ie $f(y) \le f(x) + \langle \nabla f(x) | y-x \rangle$
\end{itemize}
	
\end{otherbox}

\begin{propbox}{Le seul théorème pour prouver l'unicité}
Les minimiseurs locaux de $f$ sont des minimiseurs globaux.\\
\textbf{Si $f$ est strictement convexe, $f$ admet au plus un minimiseur.}	
\end{propbox}

\section{Conjugate}
Let $\mathcal H$ be a Hilbert space and $f : S \to [-\infty, +\infty]$.\\
The Fenchel-Legendre \textbf{conjugate} of $f$ is the function $f^* : S \to [-\infty, +\infty]$ such that : 
$$\forall\, u \in \mathcal H, \; f^*(u) = \underset{x \in \mathcal H}{sup}\, (\langle x | u \rangle - f(x))$$
List of conjugates : 
\begin{itemize}
\item if $f(x) = \langle v | x \rangle$ \quad \quad then \quad \quad $f^* = \iota_{\{v\}}$	
\item if $f = \frac12||.||^2$ \quad \quad then \quad \quad $f^* = \frac12||.||^2$
\item if $f = \iota_{[c, +\infty[}$ \quad \quad then \quad \quad $f^*(u) = \iota_{]-\infty,0]}(u) + u$
\item if $f(x) = \frac{1}{q}|x|^q$ \quad \quad then \quad \quad $f^*(u) = \frac{1}{q^*}|u|^{q^*}$	 \quad with $\frac{1}{q} + \frac{1}{q^*} = 1$
\item if $f$ is even (i.e. $f(-u) = f(u)$), then $f^*$ is even too
\item $\forall\, \alpha \in ]0,+\infty[, \; (\alpha f)^* = \alpha f^*(./\alpha)$
\item for any $(y,v) \in \mathcal H^2$ and $\alpha \in \mathbb R$, $(f(\cdot - y) + \langle \cdot | v \rangle  + \alpha)^* = f^*(\cdot - v) + \langle y | \cdot - v\rangle - \alpha$
\item Let $\mathcal G$ be a Hilbert space, and $L \in \mathcal B(\mathcal G, \mathcal H)$ be an isomorphism.\\
$(f \circ L)^* = f^* \circ (L^{-1})^*$
\item $f^*$ is lsc and convex
\item $f^*(0) = -inf\;f$
\end{itemize}


\begin{defbox}{Primal and dual problems}
Let $\mathcal H$ and $\mathcal G$ be two real Hilbert spaces.\\
Let $f : \mathcal H \to ]-\infty, +\infty]$, $g : \mathcal G \to ]-\infty, +\infty]$. Let $L \in \mathcal B(\mathcal H,\mathcal G)$.\\
The primal problem is :
$$\underset{x\in \mathcal H}{\text{minimize}}\;f(x) + g(Lx)$$ 
The dual problem is :
$$\underset{v\in \mathcal G}{\text{minimize}}\;f^*(-L^*v) + g^*(v)$$
\end{defbox}
\textit{(Ceci est une façon "complexe" et plus générique d'exprimer nos problèmes habituels de minimisation d'un produit scalaire sous contrainte d'inégalité.)}

\section{Descente de gradient}
\begin{otherbox}{Algorithme}
$$\forall\, n \in \N, \quad x_{n+1} = x_n + \lambda_n (P_{\mathcal C}(x_n - \gamma_n \nabla f(x_n))-x_n)$$	
où $\gamma_n \in \R_+^*$ et $\lambda \in ]0, 1]$.
\end{otherbox}

\textbf{Remarque} : 
$$x \text{ est un point fixe} \Longleftrightarrow \left\{\begin{array}{l}x \in \mathcal C\\\forall\,y \in \mathcal C, \; \langle \nabla f(x) | y-x \rangle \ge 0\end{array}\right.$$
\quad\\

\textbf{Remarque} : Quand il n'y a pas de contrainte, ie $\mathcal C = \mathcal H$, l'alogirthme s'exprime plus simplement : 
$$x_{n+1} = x_n - \gamma_n \nabla f(x_n)$$

\begin{propbox}{Théorème de convergence}
Assume that :
\begin{itemize}
\item $f$ is convex
\item $f$ has a Lipschitzian gradient with constant $\beta \in \R_+^*$
\item $D = \underset{x \in \mathcal C}{\text{argmin}}\,f(x) \ne \O$	
\item $\inf_{n \in \N} \, \gamma_n > 0$ and $\sup_{n \in \N}\,\gamma_n < 2 / \beta$
\item $\inf_{n \in \N}\,\lambda_n > 0$ and $\sup_{n \in \N}\,\lambda_n \le 1$
\end{itemize}
Then the sequence $(x_n)_{n \in \N}$ generated by the projected gradient algorithm :
\begin{itemize}
\item is Fejér monotone with respect to $D$, ie.	 $$(\forall\, x \in D)\,(\forall\,n \in \N) \quad ||x_{n+1} - x|| \le ||x_n - x||$$
\item converges weakly to a minimizer of $f$ over $\mathcal C$
\end{itemize}
\end{propbox}

\begin{otherbox}{Reminder : Lipschitz-continuity}
$f$ has a Lipschitzian gradient with constant $\beta \in ]0, +\infty[$ if :
$$(\forall\,(x,y)\in \mathcal H^2) \quad ||\nabla f(x) - \nabla f(y)|| \le \beta ||x-y||$$ 	
\end{otherbox}


\subsection*{Quasi-Newton algorithm}
Let $(A_n)_{n \in \N}$ be a sequence of strictly positive self-adjoint operators in $\mathcal B(\mathcal H, \mathcal H)$. The gradient algorithm reads :
$$(\forall\, n \in \N) \quad \begin{array}{rcl}x_{n+1} &=& x_n - \gamma_n A_n^{-1} \nabla f(x_n)\\ &=&x_n - \tilde A_n^{-1} \nabla f(x_n)\end{array}$$
where $\tilde A_n = \gamma_n^{-1} A_n$

\subsection*{Newton's method}
If $f$ is twice Frechet differentiable and its Hessian is strictly positive on $\mathcal H$, one can choose :
$$\forall\,n \in \N, \quad \tilde A_n = \nabla^2 f(x_n)$$

\section{Dualité}
Soient $\mathcal H$ et $\mathcal G$ deux espaces de Hilbert.\\
Soient $f : \mathcal H \to ]-\infty, +\infty]$ et $g : \mathcal G \to ]-\infty, +\infty]$ deux fonctions propres.\\
Soit $L \in \mathcal B(\mathcal H,\mathcal G)$.\\
\quad\\
Posons :
\begin{itemize}
\item $\mu = \underset{x \in \mathcal H}{inf}\,f(x) + g(Lx)$	
\item $-\mu^* = \underset{v \in \mathcal G}{sup} (-(f^*(-L^*v) + g^*(v))$
\end{itemize}

\begin{propbox}{Dualité faible}
$$\mu \ge -\mu^*$$
\end{propbox}

\begin{propbox}{Dualité forte}
Si $f$ et $g$ sont propres, convexes et lsc,\\
Si $0 \in \{y - Lx\,|\,(x,y) \in dom\,f \times \,dom\,g\}$, alors :
$$\mu = -\mu^*$$
\end{propbox}
Remarque : une condition suffisante (plus simple) est : 
$$int(dom\,g) \cap L(dom\,f) \ne \O \quad \text{ ou } \quad dom\,g \cap int(L(dom\,f)) \ne \O$$

\newpage
\begin{defbox}{Problèmes canoniques}
\begin{itemize}
	\item Primal LP : $$\underset{x \in \R_+^N}{\text{minimize}}\,\langle c | x \rangle \text{ s.t. } Lx \ge b$$
	\item Dual LP : $$\underset{x \in \R_+^N}{\text{maximize}}\,\langle b | y \rangle \text{ s.t. } L^\top y \le c$$
\end{itemize}	
\end{defbox}

\begin{otherbox}{Dualité (dans le cas linéaire)}
\begin{itemize}
\item Dualité faible : $\langle b | y \rangle \le \langle c | x \rangle$ pour $x$ Primal-réalisable et $y$ Dual-réalisable.
\item Dualité forte : Si l'une des conditions suivantes est satisfaite (elles sont équivalentes) :
	\begin{itemize}
	\item Le primal OU le dual est réalisable et borné
	\item Le primal ET le dual sont réalisables
	\item Le primal OU le dual admet une solution
	\end{itemize}
Alors $$\min_{x \in A} \, \langle c | x \rangle  = \max_{y \in \mathcal B} \, \langle b | y \rangle$$
\end{itemize}
\end{otherbox}


\section{Lagrange}
\begin{otherbox}{Problem}
Let $\mathcal H$ be a Hilbert space.\\
Let $f : \mathcal H \to ]-\infty, +\infty]$ be proper.\\
Let $(m,q) \in \N^2$.\\
For every $i \in [\![1,m]\!]$, let $g_i : \mathcal H \to \R$ and\\
for every $j \in [\![1,q]\!]$, let $h_j : \mathcal H \to \R$.\\
Let :
$$C = \left\{x \in \mathcal H, \begin{array}{l}\forall\,i \in [\![1,m]\!], \, g_i(x) = 0\\\forall\,j \in [\![1,q]\!], \, h_j(x) \le  0\end{array}\right\}$$
We want to :
$$\text{find }\hat x \in \underset{x \in C}{\text{argmin}}\;f(x)$$
\end{otherbox}

\begin{defbox}{Lagrange function}
The \textbf{Lagrange function} (or Lagrangian) associated with the above problem is defined as :
\begin{itemize}
	\item $\forall\, x \in \mathcal H$
	\item $\forall \nu \in \R^m$
	\item $\forall\, \lambda \in \R_+^q$
\end{itemize}
$$\mathcal L(x, \nu, \lambda) = f(x) + \sum_{i=1}^m \nu^{(i)}g_i(x) + \sum_{j=1}^q \lambda^{(j)}h_j(x)$$
The vectors $\nu$ and $\lambda$ are called \textbf{Lagrange multipliers}.
\end{defbox}

\begin{defbox}{Primal and Dual Lagrange functions}
The \textbf{primal Lagrange function} is : 
$$\forall\,x \in \mathcal H, \; \bar {\mathcal L}(x) = \underset{\nu \in \R^m, \lambda \in \R_+^q}{sup}\,\mathcal L(x,\nu,\lambda)$$
The \textbf{dual Lagrange function} is :
$$\forall (\nu,\lambda) \in \R^m \times \R_+^q, \; \underline{\mathcal L}(\nu,\lambda) = \underset{x \in \mathcal H}{inf}\;\mathcal L(x,\nu,\lambda)$$
\end{defbox}

Pour continuer le calcul, on peut dire que les minimisations/maximisations correspondant à ces $inf$ et $sup$ sont des problèmes d'optimisation convexes dont la solution $\tilde x$ vérifie les conditions d'optimalité :
$$\nabla_x \mathcal L(\tilde x, \nu,\lambda) = 0 $$
On en déduit $\tilde x$, puis on réécrit la fonction de Lagrange avec cette nouvelle expression de $\tilde x$.

\begin{defbox}{Problèmes primal et dual}
\begin{itemize}
\item Le problème primal : 
$$\text{minimiser }\bar {\mathcal L}(x)$$
\item Le problème dual :
$$\text{maximiser }\underline {\mathcal L}(\nu,\lambda)$$
\end{itemize}
\end{defbox}
Pour résoudre ces problèmes, on vérifie que la fonction à minimiser/maximiser est convexe (ou à défaut, s'y ramener) et différentiable, puis annuler ce gradient. Il peut apparaître ici (souvent ?) une inversion de matrice, ce qui est généralement coûteux en grande dimension.

\begin{defbox}{Saddle point}
$(\hat x, \hat \nu, \hat \lambda) \in \mathcal H \times \R^m \times \R_+^q$ is a \textbf{saddle point} of $\mathcal L$ if :
$$\forall\, (x, \nu, \lambda) \in \mathcal H \times \R^m \times \R_+^q, \quad \mathcal L(\hat x, \nu, \lambda) \le \mathcal L(\hat x, \hat \nu, \hat \lambda) \le \mathcal L(x, \hat \nu, \hat \lambda)$$
\end{defbox}

\section{Résoudre un problème d'optimisation avec Lagrange}
\begin{itemize}
\item Poser une fonction $f$ et un ensemble $\mathcal C \subset \R^n$
\item Formuler le problème : $$\underset{x \in \mathcal C}{\text{minimiser}}\;f(x) \text{ où } C = \dots$$
\item Vérifier que :
	\begin{itemize}
		\item $f$ est une fonction strictement convexe coercive
		\item $C$ est un ensemble convexe fermé non vide
	\end{itemize}
	Donc le problème d'optimisation admet une unique solution.
\item Poser les fonctions qui matérialisent les contraintes : $g_i$ pour les contraintes d'égalité, et $h_j$ pour les contraintes d'inégalité.
\item Trouver un point $\bar x \in int(dom\;f))$ qui vérifie les conditions de Slater, ie : $$\left\{\begin{array}{l}\forall\, i \in [\![1,m]\!], \; g_i(\bar x) = 0\\\forall\, j \in [\![1,q]\!], \; h_j(\bar x) < 0\end{array}\right.$$
\item Définir le lagrangien $\mathcal L(x, \nu, \lambda)$ où $\nu \in \R^m$ et $\lambda \in \R_+^q$ (cf. la définition ci-dessus).
\item "On sait que $(\hat x, \hat \nu, \hat \lambda) \in \R^n \times R^m \times R_+^q$ est un point selle du Lagrangien si et seulement si \\$\hat x = \left(\begin{array}{ccc}\hat x_1 & \dots & \hat x_2\end{array}\right)^\top$ est le (un ?) minimiseur de $f$ sur $C$." \textit{(Ecrire cela)}
\item Calculer alors le gradient du Lagrangien et l'annuler, ie $\nabla_x \mathcal L(\hat x, \hat \nu, \hat \lambda) = 0$ \\En déduire $n$ expressions des $n$ composantes de $\hat x$ en fonction de $\hat \nu$ et $\hat \lambda$.
\item Utiliser les contraintes de complémentarité (vérifiées grâce aux conditions de Slater), \\
ie $\forall\,j \in [\![1,q]\!], \; \hat \lambda^{(j)}h_j(\hat x) = 0$.\\
Vu qu'on connaît l'expression des $\hat x_j$, on peut en déduire les expressions des $\hat \lambda^{(j)}$ et $\hat \nu^{(i)}$.
\item Connaissant (en théorie) toutes les valeurs, on peut écrire concrètement $f$ et résoudre le problème.
\end{itemize}

\section{Si on n'a pas la convexité}
\begin{propbox}{Karush-Kuhn-Tucker theorem}
Assume that $f$, $(g_i)_{1 \le i \le m}$ and $(h_j)_{1 \le j \le q}$ are continuously differentiable on $\mathcal H = \R^N$.\\
Assume that $\hat x$ is a local minimizer of $f$ over $\mathcal C$ satisfying the following Mangasarian-Fromovitz constraint qualification conditions :
\begin{itemize}
\item $\{\nabla g_i(\hat x) | i \in [\![1,m]\!]\}$ is a family of linear independent vectors
\item there exists $z \in \R^N$ such that :
$$\begin{array}{rcl}
(\forall\, i \in [\![1,m]\!]) & & \langle \nabla g_i(\hat x) | z \rangle = 0\\
(\forall \, j \in [\![1,q]\!] \text{ s.t. } h_j(\hat x) = 0) & & \langle \nabla h_j(\hat x) | z \rangle < 0
\end{array}$$
\textit{NB : cela revient aux conditions de Slater.}
\end{itemize}
Then there exist $\hat \nu \in \R^N$ and $\hat \lambda \in \R_+^q$ such that $\hat x$ is a critical point of $\mathcal L(\cdot, \hat \nu, \hat \lambda)$  (ie. $\nabla_x \mathcal L(\hat x, \hat \nu, \hat \lambda) = 0$ and the complementarity slackness condition holds (ie. $\forall j \in [\![1,q]\!], \, \hat \lambda h_j(\hat x) = 0$)
\end{propbox}

\begin{otherbox}{Sens "suffisant" de ce théorème}
Soit $P$ un problème d'optimisation vérifiant les hypothèses	, si $\mathcal C$ est tel que :
\begin{itemize}
\item $f$ est convexe
\item les contraintes d'égalité sont affines
\item les contraintes d'inégalité sont convexes et $\mathcal C^1$	
\end{itemize}
Alors si $\hat x$ vérifie les conditions du KKT ie $\hat x$ solution du Lagrangien, alors $\hat x$ est solution du problème d'optimisation $P$.
\end{otherbox}

\textbf{Remarque} : une condition suffisante pour avoir le résultat du KKT sans prouver les conditions de Mangasarian-Fromovitz est de vérifier si $f$, $(g_i)_{1 \le i \le m}$ et $(h_j)_{1 \le j \le q}$ sont définies sur un ouvert non vide $D \subset \R^N$.\\
Ou alors de regarder si la famille $\{\nabla g_i(\hat x) | i \in [\![1,m]\!]\} \cup \{\nabla h_j(\hat x) | j\in J(\hat x)\}$ est une famille libre.

\newpage
\subsection*{Méthode}
\begin{itemize}
\item Identifier $f$, $g_i$ et $h_j$
\item "Soit $\hat x$ un minimiseur local de $f$ sur $\mathcal C$. En particulier, $\hat x \in \mathcal C$ donc $g_i(\hat x) = 0$ et $h_j(\hat x) \le 0$
\item On montre que la famille $(g_i(\hat x))_i$ est libre. En général, l'ensemble est vide ou il n'y a qu'un seul élément donc c'est facile. Sinon, regarder la ligne du dessus et essayer de prouver le résultat.
\item Regarder si la famille $(\nabla g_i(\hat x), \nabla h_j(\hat x))$ est (par miracle) libre ; si c'ets le cas, sauter les 2 points suivants
\item A l'aide des contraintes d'égalité et d'inégalité, essayer d'en déduire des propriétés \\sur $J(\hat x) = \{j \in [\![1,q]\!], \; h_j(\hat x) = 0\}$. Avec un peu de chance, cet ensemble est vide.
\item Trouver $z$ tel que $\forall \, j \in J(\hat x), \, \langle \nabla h_j(\hat x) | z \rangle < 0$ et $\langle \nabla g_i(\hat x) | z \rangle = 0$
\item "Par le théorème KKT, il existe $\hat \nu$ et $\hat \lambda$ tels que $\nabla_x \mathcal L(\hat x, \hat \nu, \hat \lambda) = 0$ et les conditions de détente sont vérifiées.
\item Résoudre le système d'équations (et inéquations). \textit{(\`A skip ?)}
\end{itemize}

\section{Vrac}
\begin{itemize}
\item $f$ propre, lsc et coercive $\implies$ au moins 1 minimiseur
\item $f$ strictement convexe $\implies$ au + 1 minimiseur
\item Problème convexe $\Longleftrightarrow$ contraintes d'inégalités linéaires  + fonction de coût convexe.
\item $f$ continue $\implies$ $f$ lsc
\item Quelle est la forme de l'ensemble des $x$ tels que $Ax \ge b$ ? $99\%$ du temps, ce sera un polyhèdre.\\
Défintion d'un polyhèdre : intersection finie de demis espaces fermés.
\end{itemize}

\end{document}
